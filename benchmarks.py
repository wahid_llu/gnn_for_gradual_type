import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from dataset import data_loader as loader
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.svm import SVR, SVC
from sklearn.neural_network import MLPClassifier, MLPRegressor
from sklearn.ensemble import RandomForestRegressor,AdaBoostRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error as MSE
from sklearn.metrics import mean_absolute_error as MAE
from sklearn.datasets import load_iris
from alipy.experiment.al_experiment import AlExperiment
from math import sqrt
import pickle
import warnings
warnings.filterwarnings("ignore")


# Seed = 0
# np.random.seed(Seed)

X, y = loader.meteor()

CROSS_VALIDATION = 10

MODEL = [LinearRegression(), SVR(), RandomForestRegressor(),
         DecisionTreeRegressor(), AdaBoostRegressor(), MLPRegressor()]


def ratioSummary(true_arr, pred_arr, ratio = [.05, .1, .15, 1]):
    pred_arr = pred_arr.reshape(-1, 1)
    abs_diff = np.abs(true_arr - pred_arr)
    abs_diff_ratio = np.divide(abs_diff, true_arr)
    count = []
    for rt in ratio:
        count.append(np.sum(abs_diff_ratio < rt))
    count[1] = count[1] - count[0]
    count[2] = count[2] - count[1] - count[0]
    count[3] = count[3] - count[2] - count[1] - count[0]
    return count





for model in MODEL:
    Loss_RMSE, Loss_MAE, y_AVG = [], [], []
    for cv in range(CROSS_VALIDATION):
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.7)
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        rmse = sqrt(MSE(y_test, y_pred))
        mae = MAE(y_test, y_pred)
        # print(loss)
        Loss_RMSE.append(rmse)
        Loss_MAE.append(mae)
        y_AVG.append(y_test.mean())
    print(model)
    print('MSE (sec.): mean={:4f}; std={:4f}'.format(np.array(Loss_RMSE).mean(), np.array(Loss_RMSE).std()))
    print('MSE (ratio): {:4%}'.format(np.array(Loss_RMSE).mean() / np.array(y_AVG).mean()))
    print('MAE (sec.): mean={:4f}; std={:4f}'.format(np.array(Loss_MAE).mean(), np.array(Loss_MAE).std()))
    print('MAE (ratio): {:4%}'.format(np.array(Loss_MAE).mean() / np.array(y_AVG).mean()))
    count = ratioSummary(y_test, y_pred)
    print('Error Count by Ratio :\n  <5%: {:d};\n  '
          '5%-10%: {:d};\n  10%-15%: {:d};\n  >15%: {:d}'.format(count[0], count[1], count[2], count[3]))
    print()
    print()



