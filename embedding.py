import torch
from dataset import data_loader as loader
from torch_geometric.data import Data
from torch_geometric.datasets import TUDataset
from torch_geometric.datasets import Planetoid
import torch.nn.functional as F
from torch.nn.functional import mse_loss as MSELoss
from torch.nn.functional import l1_loss as MAELoss
from methods.network import Net, GCN_Predict, TACA, Encoder
from utils.util import get_init_idx, after_random_quired_idx, \
    after_kcenter_quired_idx, get_unlabeled_logits, get_init_idx_k_center_greedy, \
    aggregation_function, train_test_split
import torch_geometric.transforms as T
from torch_geometric.nn import GCNConv, GAE, VGAE
from utils.train_test_split_edges import train_test_split_edges
import numpy as np
from methods.Queryer import K_Center_Greedy
import pickle
import time

torch.manual_seed(0)

N_Query =50
maxEmbeddingEpoch = 200
N_Init = 30
HOLD_OUT_RATIO = 0.1
DATASET = 'RayTrace'
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


if DATASET == 'RayTrace':
    X, y = loader.meteor()
    from dataset import generateMeteor as generator
elif DATASET == 'Nbody':
    X, y = loader.nbody()
    from dataset import generatenbody as generator
elif DATASET == 'RayTrace':
    X, y = loader.raytrace()
    from dataset import  generateRayTrace as generator

graph = generator.callgraph
train_pos_edge_index = torch.tensor(np.array(graph), dtype=torch.long).to(device)
num_node, num_feature = int(np.amax(graph)+1), X.shape[1]
test_mask = train_test_split(X.shape[0], ratio = HOLD_OUT_RATIO)



Net_Architecture = [num_feature,
                    8, 4, 1] # number of neurons in output layer is 1 for regression
model = VGAE(Encoder(Net_Architecture[0], Net_Architecture[1])).to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.001, weight_decay=5e-4)

def embedding():
    '''
        Phase I: Embedding
        Each config (bit-vector) is a graph, whose topology is given
        by the call-graph and each node is a sparse, sub-bit-vector.
        %
        We want to embed such graphs, representing the nodes into a low-dimensional space
        while preserving the caller-callee relations.
    '''
    embedded_X = []
    for id_config, x in enumerate(X):
        x = generator.bit_vector_to_bit_matrix(list(x))
        x = torch.tensor(np.array(x), dtype=torch.long).to(device)
        for epoch in range(1, maxEmbeddingEpoch):
            model.train()
            optimizer.zero_grad()
            z = model.encode(x.float(), train_pos_edge_index)
            loss = model.recon_loss(z, train_pos_edge_index)
            loss = loss + (1 / X.shape[0]) * model.kl_loss()
            loss.backward()
            optimizer.step()
        embedded_X.append(aggregation_function(z.detach()))
    return torch.stack(embedded_X)

start_time = time.time()
embedded_X = embedding()
embedding_time = time.time()

with open(str(DATASET)+'_embedded.result', 'wb') as f:
    pickle.dump(embedded_X, f)


print()
print("embedding takes {:.2f} seconds".format(embedding_time - start_time))



