import functools
import subprocess
callgraph = [[0, 0, 0, 0, 0, 0, 8, 9, 2, 2, 2, 10, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 11, 11, 12, 12, 12, 12, 12, 5, 5, 5, 6, 6, 6, 6, 6, 6], [1, 2, 3, 4, 5, 6, 7, 7, 7, 8, 10, 7, 7, 8, 10, 1, 2, 7, 10, 3, 11, 12, 8, 3, 7, 8, 10, 3, 11, 8, 4, 3, 7, 8, 9, 10, 1, 3]]

def generate_program(bit_vector):
    bit_vector_to_numstring = functools.reduce((lambda x, y: x + y), map(str, bit_vector))
    cmd = ['racket', 'setup-benchmark.rkt', 'gregor', bit_vector_to_numstring]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    p.wait()
    cmd = ['racket', 'run.rkt', 'gregor']
    p1 = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    time_string = ""
    for line in p1.stdout:
        time_string = line
    stripped_time_string = time_string.strip()
    time = float(stripped_time_string)
    #convert time to seconds
    time = time / 1000    
    p1.wait()
    return time

def bit_vector_to_bit_matrix(bit_vector):
    # initializer an nxn bit_matrix for a vector in n dimensions
    bit_matrix = [[0 for _ in range(len(bit_vector))] for _ in range(len(bit_vector))]
    for i in range(len(bit_vector)):
        # copy the diagonal
        bit_matrix[i][i] = bit_vector[i]
    return bit_matrix

def bit_matrix_to_bit_vector(bit_matrix):
    return [bit_matrix[i][i] for i in range(len(bit_matrix))]
        

if __name__ == "__main__":
    bit_vector = [0,0,1,1,0,0,1,0,1,1,0,1,1]
    bit_matrix = bit_vector_to_bit_matrix(bit_vector)
    print(len(bit_vector))
    print(bit_vector == bit_matrix_to_bit_vector(bit_matrix))
    #print(generate_program())
