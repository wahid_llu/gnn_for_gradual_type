import random
import subprocess

callgraph = [[6, 5, 5, 4, 4, 2, 3], [5, 0, 4, 2, 3, 1, 1]]
def generate_header(function_name, param_names, param_anns, sub_vector):
    """
    String List[String] List[String] Slice[Bit] -> String
    This function accepts a function name, its corresponding parameter names,
    the possible annotations for these parameters, and a slice of the global
    parameter bit vector that corresponds to the parameter indexes.    
    It produces a header with proper type annotations added
    """
    params = [(param_names[index] + param_anns[index] if value else param_names[index]) for index, value in enumerate(sub_vector)]
    param_string = ",".join(params)
    header = "def {0}({1})->Dyn:".format(function_name, param_string)
    return header

def generate_function(function_name, param_names, param_anns, function_body):
    """
    String List[String] List[String] -> (Slice[bit] -> String)
    This function generates a function body with annotations specified by sub_vector
    """
    return (lambda sub_vector: generate_header(function_name, param_names, param_anns, sub_vector) + function_body)
header = """from array import array
import math

#import perf
from six.moves import xrange
import time



#Assign the function array2d the type: Function(['w:Dyn', 'h:Dyn', 'data:Dyn'], Tuple(Dyn,Dyn,Dyn))
#Assign the function idx the type: Function(['arr:Tuple(Int,Int,Dyn)', 'x:Int', 'y:Choice c<Dyn, Int>'], Int)
#Assign the function getitem the type: Function(['arr:Tuple(Dyn,Dyn,Dyn)', 'x_y:Tuple(Int,Choice c<Dyn, Int>)'], Dyn)
#Assign the function setitem the type: Function(['arr:Tuple(Int,Int,Dyn)', 'x_y:Tuple(Int,Choice c<Dyn, Int>)', 'val:Dyn'], Tuple(Dyn,Dyn,Dyn))
#Assign the function SOR_execute the type: Function(['omega:Choice o<Dyn, Float>', 'G:Tuple(Dyn,Dyn,Choice f<Dyn, Float>)', 'cycles:Choice q<Dyn, Int>'], Void)
#Assign the function bench_SOR the type: Function(['loops:Choice g<Dyn, Int>', 'n:Dyn', 'cycles:Int'], Void)
#Assign the function main the type: Function([], Void)

#def array2d(w, h):

"""
array2d_header = "def array2d(w, h)->Tuple(Dyn,Dyn,Dyn):"
array2d_params = "w, h".split(',')
array2d_anns = [":int", ":int"]
array2d_body = """
    d = array('d', [0.0]) * (w * h)
    #if data is not None:
    #    return setup(d,data)
    return (w, h, d)

"""
generate_array2d = generate_function("array2d", array2d_params, array2d_anns, array2d_body)

idx_headers = ["def idx(arr, x, y):",
    "def idx(arr, x, y) -> int:",
    "def idx(arr, x:int, y) -> int:",
    "def idx(arr, x, y:int) -> int:",
    "def idx(arr, x:int, y:int) -> int:",
    "def idx(arr:Tuple(int,int,Dyn), x ,  y)->int:",
    "def idx(arr:Tuple(int,int,Dyn), x : int,  y:int)->int:"
]
idx_params = "arr, x, y".split(",")
idx_anns = [":Tuple(int,int,int)", ":int", ":int"]
idx_body = """
    w,h,d = arr    
    if 0 <= x < (w+0) and 0 <= y < (h+0):
        return (y+0) * (w+0)  + x + 0
    raise IndexError
    return 0

"""
generate_idx = generate_function("idx", idx_params, idx_anns, idx_body)

getitem_headers = [
    "def getitem(arr:Tuple(Dyn,Dyn,Dyn), x_y):",
    "def getitem(arr, x_y:Tuple(Dyn,Dyn)):",
    "def getitem(arr, x_y:Tuple(int,Dyn)):",
    "def getitem(arr:Tuple(Dyn,Dyn,Dyn), x_y:Tuple(int,Dyn)):",
    "def getitem(arr, x_y:Tuple(Dyn,int)):",
    "def getitem(arr:Tuple(Dyn,Dyn,Dyn), x_y:Tuple(Dyn,int)):",
    "def getitem(arr:Tuple(Dyn,Dyn,Dyn), x_y:Tuple(int,int)): "
]
getitem_params = "arr, x_y".split(",")
getitem_anns = [":Tuple(Dyn,Dyn,Dyn)", "x_y:Tuple(int,int)"]
getitem_body = """
    w,h,d = arr
    (x, y) = x_y
    return d[idx(arr, x, y)]

"""
generate_getitem = generate_function("getitem", getitem_params, getitem_anns, getitem_body)

setitem_headers = [
    "def setitem(arr, x_y, val)->Tuple(Dyn,Dyn,Dyn):",
    "def setitem(arr:Tuple(Dyn,Dyn,Dyn), x_y, val)->Tuple(Dyn,Dyn,Dyn):",
    "def setitem(arr, x_y:Tuple(Dyn,Dyn), val)->Tuple(Dyn,Dyn,Dyn):",
    "def setitem(arr, x_y:Tuple(int,Dyn), val)->Tuple(Dyn,Dyn,Dyn):",
    "def setitem(arr:Tuple(Dyn,Dyn,Dyn), x_y:Tuple(int,Dyn), val)->Tuple(Dyn,Dyn,Dyn):",
    "def setitem(arr, x_y:Tuple(Dyn,int), val)->Tuple(Dyn,Dyn,Dyn):",
    "def setitem(arr:Tuple(Dyn,Dyn,Dyn), x_y:Tuple(Dyn,int), val)->Tuple(Dyn,Dyn,Dyn):",
    "def setitem(arr:Tuple(int,int,Dyn), x_y:Tuple(int,int), val)->Tuple(Dyn,Dyn,Dyn):   "
    ]
setitem_params = "arr, x_y, val".split(",")
setitem_anns = ["::Tuple(int,int,Dyn)", ":Tuple(int,int,int)", ":float"]
setitem_body = """
    (x, y) = x_y
    w, h, d = arr    
    d[idx(arr, x, y)] = val    
    return (w, h, d)

"""
generate_setitem = generate_function("setitem", setitem_params, setitem_anns, setitem_body)
    
SOR_execute_headers = [
    "def SOR_execute(omega, G, cycles):",
    "def SOR_execute(omega:float, G, cycles):",
    "def SOR_execute(omega, G:Tuple(int,int,Dyn), cycles):",
    "def SOR_execute(omega, G, cycles:int):",
    "def SOR_execute(omega:float, G:Tuple(int,int,Dyn), cycles):",
    "def SOR_execute(omega:float, G:Tuple(int,int,Dyn), cycles:int):"]
SOR_execute_params = "omega, G, cycles".split(",")
SOR_execute_anns = [":float", ":Tuple(int,int,Dyn)", ":int"]
SOR_execute_body = """
    w,h,d = G
    for p in xrange(cycles+0):
        for y in xrange(1, h - 1):
            for x in xrange(1, w - 1):                
                G = setitem(G,(x, y), (omega * 0.25 * (getitem(G,(x, y - 1)) + getitem(G,(x, y + 1)) + getitem(G,(x - 1, y))
                                                     + getitem(G,(x + 1, y))))
                           + (1.0 - omega) * getitem(G,(x, y)))
    return None

"""
generate_SOR_execute = generate_function("SOR_execute", SOR_execute_params, SOR_execute_anns, SOR_execute_body)
bench_SOR_headers = [
    "def bench_SOR(loops, n, cycles):",
    "def bench_SOR(loops:int, n, cycles):",
    "def bench_SOR(loops, n:int, cycles):",
    "def bench_SOR(loops, n, cycles:int):",
    "def bench_SOR(loops:int, n, cycles:int):",
    "def bench_SOR(loops:int, n:int, cycles:int):"]
bench_SOR_body = """
    range_it = xrange(loops+0)
    t0 = time.time()#perf.perf_counter()

    for _ in range_it: 
        G = array2d(n, n)
        SOR_execute(1.25, G, cycles+0)#, Array)
    t1 = time.time()
    print(t1 - t0)
    return None
    #return t1 - t0#perf.perf_counter() - t0

"""
bench_SOR_params = "loops, n, cycles".split(",")
bench_SOR_anns = [":int", ":int", ":int"]
generate_bench_SOR = generate_function("bench_SOR", bench_SOR_params, bench_SOR_anns, bench_SOR_body)
rest = """

def main():
    bench_SOR(4,10,1)

main()

"""
array2d_start = 0
array2d_end = len(array2d_params)
idx_start = array2d_end
idx_end = idx_start + len(idx_params)
getitem_start = idx_end
getitem_end = getitem_start + len(getitem_params)
setitem_start = getitem_end
setitem_end = setitem_start + len(setitem_params)
SOR_execute_start = setitem_end
SOR_execute_end = SOR_execute_start + len(SOR_execute_params)
bench_SOR_start = SOR_execute_end
bench_SOR_end = bench_SOR_start + len(bench_SOR_params)

def generate_program(bit_vector):
    with open("SOR_test.py", "w+") as f:
        array2d = generate_array2d(bit_vector[array2d_start:array2d_end])
        idx = generate_idx(bit_vector[idx_start:idx_end])
        getitem = generate_getitem(bit_vector[getitem_start:getitem_end])
        setitem = generate_setitem(bit_vector[setitem_start:setitem_end])
        SOR_execute = generate_SOR_execute(bit_vector[SOR_execute_start:SOR_execute_end])
        bench_SOR = generate_bench_SOR(bit_vector[bench_SOR_start:bench_SOR_end])
        program = header + array2d + idx + getitem + setitem + SOR_execute + bench_SOR + rest
        f.write(program)

    cmd = ['retic', '--guarded', 'SOR_test.py']
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    time_string = ""
    for line in p.stdout:
        time_string = line
    stripped_time_string = time_string.strip()
    time = float(stripped_time_string)
    #print(time)
    p.wait()
    return time

def copy_range(bit_vector, start, end):
    row = [0] * bench_SOR_end
    for i in range(start, end):
        row[i] = bit_vector[i]
    return row

def bit_vector_to_bit_matrix(bit_vector):
    copy_curry = lambda start, end: copy_range(bit_vector, start, end)
    array2d = copy_curry(array2d_start, array2d_end)
    idx = copy_curry(idx_start, idx_end)
    getitem = copy_curry(getitem_start, getitem_end)
    setitem = copy_curry(setitem_start, setitem_end)
    SOR_execute = copy_curry(SOR_execute_start, SOR_execute_end)
    bench_SOR = copy_curry(bench_SOR_start, bench_SOR_end)
    main = [0] * bench_SOR_end
    return [array2d, idx, getitem, setitem, SOR_execute, bench_SOR, main]

def bit_matrix_to_bit_vector(bit_matrix):
    vector_from_matrix = [0] * len(bit_matrix[0])
    for row in bit_matrix:
        for index, el in enumerate(row):
            vector_from_matrix[index] |=  el
    return vector_from_matrix

def main():
    indexList = []
    i = 1
    while i <= 1024:

        idx_index = random.randrange(0, len(idx_headers))
        getitem_index = random.randrange(0, len(getitem_headers))
        setitem_index = random.randrange(0,len(setitem_headers))
        SOR_execute_index = random.randrange(0, len(SOR_execute_headers))
        bench_SOR_index = random.randrange(0, len(bench_SOR_headers))

        if not (idx_index, getitem_index, setitem_index, SOR_execute_index, bench_SOR_index) in indexList:
            indexList.append((idx_index, getitem_index, setitem_index, SOR_execute_index, bench_SOR_index))
            i += 1
            idx = idx_headers[idx_index]  + idx_body
            getitem = getitem_headers[getitem_index]  + getitem_body
            setitem = setitem_headers[setitem_index]  + setitem_body
            SOR_execute = SOR_execute_headers[SOR_execute_index]  + SOR_execute_body
            bench_SOR = bench_SOR_headers[bench_SOR_index]  + bench_SOR_body

            program = header +  idx + getitem + setitem + SOR_execute + bench_SOR + rest
            programName = "SOR" + str(i) + ".py"
            with open(programName, 'w+') as f:
                f.write(program)

if __name__ == "__main__":
    bit_vector = [random.randrange(0, 2) for _ in range(bench_SOR_end)]
    print(bench_SOR_end)
    print(len([0,0,0,0,1,0,1,1,0,0,0,0,1,1,1,1]))
    print(len(bit_vector) == len([0,0,0,0,1,0,1,1,0,0,0,0,1,1,1,1]))
    #generate_program(bit_vector)
    matrix = bit_vector_to_bit_matrix(bit_vector)
    print(bit_vector == bit_matrix_to_bit_vector(matrix))
    print(len(matrix))

            
