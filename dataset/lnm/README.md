lnm-plot
========

Scripts implementing the data analysis for the paper.
Computes a set of `L-N/M` plots for a dataset+modulegraph and converts the data
 to a spreadsheet.

Notes
-----
- Performs best when gradually typed!
  - Both `lnm-plot.rkt` and `summary.rkt` interact with typed libraries.
