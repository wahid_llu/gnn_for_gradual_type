import functools
import subprocess
callgraph = [[0], [1]]
def generate_program(bit_vector):
    bit_vector_to_numstring = functools.reduce((lambda x, y: x + y), map(str, bit_vector))
    cmd = ['racket', 'setup-benchmark.rkt', 'sieve', bit_vector_to_numstring]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    p.wait()
    cmd = ['racket', 'run.rkt', 'sieve']
    p1 = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    time_string = ""
    for line in p1.stdout:
        time_string = line
    stripped_time_string = time_string.strip()    
    time = float(stripped_time_string)
    #convert time to seconds
    time = time / 1000
    print(time)
    p1.wait()
    return time

def bit_vector_to_bit_matrix(bit_vector):
    main = [bit_vector[0], 0]
    streams = [0, bit_vector[1]]
    return [main, streams]

if __name__ == "__main__":
    generate_program([0, 0])
