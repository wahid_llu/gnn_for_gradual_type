import torch
from dataset import data_loader as loader
from dataset import pytorchgemetric_dataset as Dataset
from torch_geometric.data import DataLoader
from torch.nn import Linear
import torch.nn.functional as F
from torch_geometric.nn import GCNConv
from torch_geometric.nn import global_mean_pool
from torch_geometric.nn import GraphConv
from torch_geometric.nn import GINConv
import torch.nn as nn
from math import sqrt
import random
import numpy as np
# Load the bit vectors
X, y = loader.nbody()
print(len(X),len(X[0]),len(y))
print(X[0],type(y[0]))

# Load the call graph
from dataset import generatenbody as generator 
graph = generator.callgraph
print(graph)
is_python_data=True
# Generate graph dataset using bit vectors and call graph
dataset=Dataset.generategraphdataset(X,y,graph,generator,is_python_data)
tempdata=dataset[10]
number_of_node_features=len(tempdata.x[0])
print(tempdata)
random.shuffle(dataset)

# Train test split
number_of_graph_in_training=100
train_dataset = dataset[:number_of_graph_in_training]
test_dataset = dataset[number_of_graph_in_training:]
train_loader = DataLoader(train_dataset, batch_size=32, shuffle=True)
test_loader = DataLoader(test_dataset, batch_size=32, shuffle=False)
print(len(train_loader.dataset))
for step, data in enumerate(train_loader):
    print(f'Step {step + 1}:')
    print('=======')
    print(f'Number of graphs in the current batch: {data.num_graphs}')
    print(data)
    print()

# Graph Isomorphism network
class GIN(torch.nn.Module):
    def __init__(self, hidden_channels):
        super(GIN, self).__init__()
        torch.manual_seed(12345)
        self.conv1 = GINConv( nn.Sequential(nn.Linear(number_of_node_features, hidden_channels),
                                  nn.ReLU(), nn.Linear(hidden_channels, hidden_channels)))
        self.conv2 = GINConv(nn.Sequential(nn.Linear(hidden_channels, hidden_channels),
                                  nn.ReLU(), nn.Linear(hidden_channels, hidden_channels)))
        self.conv3 = GINConv(nn.Sequential(nn.Linear(hidden_channels, hidden_channels),
                                  nn.ReLU(), nn.Linear(hidden_channels, hidden_channels)))
        self.lin = Linear(hidden_channels, 1)
        
        

    def forward(self, x, edge_index, batch):
        
        # 1. Obtain node embeddings 
        x = self.conv1(x, edge_index)
        x = x.relu()
        x = self.conv2(x, edge_index)
        x = x.relu()
        x = self.conv3(x, edge_index)
        x=x.to('cpu')
        # 2. Readout layer
        x = global_mean_pool(x, batch)  # [batch_size, hidden_channels]

        # 3. Apply a final classifier
        x = F.dropout(x, p=0.05, training=self.training)
        x=x.to(device)
        x = self.lin(x)
        
        return x.squeeze(1)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = GIN(hidden_channels=64).to(device)
print(model)
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
criterion = torch.nn.L1Loss()

def train():
    model.train()

    for data in train_loader:  # Iterate in batches over the training dataset.
         out = model(data.x, data.edge_index, data.batch)  # Perform a single forward pass.
         loss = criterion(out, data.y)  # Compute the loss.
         #print(f'Loss is {loss.item()}')
         loss.backward()  # Derive gradients.
         optimizer.step()  # Update parameters based on gradients.
         optimizer.zero_grad()  # Clear gradients.
def test(loader):
     model.eval()

     correct = 0
     for data in loader:  # Iterate in batches over the training/test dataset.
         out = model(data.x, data.edge_index, data.batch)  
         pred = out  # Use the class with highest probability.
         #correct += int((pred == data.y).sum())  # Check against ground-truth labels
        
         correct += (abs((pred-data.y))).sum()
     return correct / len(loader.dataset)  # Derive ratio of correct predictions.
def avgerror(test_loader):
    model.eval()
    loss_func = torch.nn.L1Loss()
    loss_func2= torch.nn.MSELoss()
    
    total_loss=0
    total_mse=0
    for batch in test_loader:
        out = model(batch.x, batch.edge_index, batch.batch)
        pred=out
        label = batch.y
        loss = loss_func(pred, label)
        loss_mse=loss_func2(pred, label)
        total_loss += loss.item()
        total_mse+=loss_mse.item()
    total_loss/= len(test_loader.dataset)
    total_mse/=len(test_loader.dataset)
    total_mse=sqrt(total_mse)
    print(total_loss)
    print(total_mse)
def errorratio_detail(errorarr):
    
    count_bellow_5=0
    count_bellow_10=0
    count_bellow_15=0
    count_above_15=0
    total=len(errorarr)
    print(type(errorarr))
    print(total)
    
   
    for a in errorarr:
        
        if a <=0.05:
            count_bellow_5+=1
        elif a>0.05 and a<=0.1:
            count_bellow_10+=1
        elif a>0.1 and a<=.15:
            count_bellow_15+=1
        else:
            count_above_15+=1
    error_less_than_5=(count_bellow_5/total)*100
    error_less_than_10=(count_bellow_10/total)*100
    error_less_than_15=(count_bellow_15/total)*100
    error_above_15=(count_above_15/total)*100
    print("Total number of item is {}".format(total))
    print("Number of value bellow 5% is {}".format(count_bellow_5))
    print("Number of value bellow 10% is {}".format(count_bellow_10))
    print("Number of value bellow 15% is {}".format(count_bellow_15))
    print("Number of value above 15% is {}".format(count_above_15))
    print("Error ratio less than 5% is {}".format(round(error_less_than_5,4)))
    print("Error ratio less than 10% is {}".format(round(error_less_than_10,4)))
    print("Error ratio less than 15% is {}".format(round(error_less_than_15,4)))
    print("Error ratio more than than 15% is {}".format(round(error_above_15,4)))
def predicttime(arr):
    x=np.array(arr)
    y=np.random.rand(len(arr),1)
    for a in x:
        if len(a)!=len(X[0]):
            print("Data dimension did not match")
            return
    data_sample=Dataset.generategraphdataset(x,y,graph,generator,is_python_data)
    print(data_sample[0])
    loader = DataLoader(data_sample, batch_size=1, shuffle=True)
    for batch in loader:
        out = model(batch.x, batch.edge_index, batch.batch)
        print(out)
    
def errorratio(test_loader):
    correct = 0
    result=0
    error_arr=[]
    for batch in test_loader:
        out = model(batch.x, batch.edge_index, batch.batch)
        pred=out
        temp_arr=abs((pred-batch.y))/batch.y
        error_arr.append(temp_arr.tolist())
        correct += (abs((pred-batch.y))/batch.y).sum()
    
    result=correct / len(test_loader.dataset)
    print(result.item())
    flatten_list = sum(error_arr, [])
    errorratio_detail(flatten_list)
for epoch in range(1, 201):
    train()
    train_acc = test(train_loader)
    test_acc = test(test_loader)
    print(f'Epoch: {epoch:03d}, Train Error: {train_acc:.4f}, Test Error: {test_acc:.4f}')
avgerror(test_loader)
errorratio(test_loader)
#Sample data to predict time
arr=[
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,1,0,0,1,0,0,0,0,0,1,0,0,0,0,0]
]
predicttime(arr)