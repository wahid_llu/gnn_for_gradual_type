from dataset import data_loader as loader
import numpy as np
X, y = loader.suffixtree()
f = open("performence_latice.txt", "w")
for i in range(len(y)):
    print(X[i],end=' ')
    print('{:0.2f}X'.format(y[i].item(0)/y[0].item(0)))
    st=np.array2string(X[i],precision=2, separator=' ')
    f.write(st)
    f.write(' {:0.2f}X \n'.format(y[i].item(0)/y[0].item(0)))
f.close() 
