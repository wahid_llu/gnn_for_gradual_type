from alipy.index import IndexCollection
import numpy as np
from sklearn.metrics import mean_squared_error as MSE
from sklearn.model_selection import train_test_split
from math import sqrt

n_per_quary = 3

def RMSE(y, y_):
    return np.sqrt(np.square(y.ravel() - y_))

# def get_query_idx(error_list, idx_collection):
#     uncertain_idx = error_list.argsort()[-n_per_quary:][::-1]
#     query_idx = []
#     for i in uncertain_idx:
#         query_idx.append(idx_collection[i])
#     return IndexCollection(query_idx)

def get_query_idx(pred_matrix, idx_collection):
    pred_std = np.std(np.array(pred_matrix), axis=0)
    uncertain_idx = pred_std.argsort()[-n_per_quary:][::-1]
    query_idx = [idx_collection[i] for i in uncertain_idx]
    return IndexCollection(query_idx)

class MaxUncertainQBC():

    def __init__(self, X, y, StartRatio=0.1, n_Query=50, TestSize=0.1):
        self.X, self.X_test, self.y, self.y_test = train_test_split(X, y, test_size=TestSize)
        self.ratio = StartRatio
        self.N = n_Query
        self.loss = []
        self.sampleSize = []
        self.committe_predict=[]
    #takes input and print the predicted time, actual time & MSE 
    def predict_result(self,test,y_test1):
        y_pred1 = np.zeros(y_test1.shape)
        for learner in self.committe_predict: 
            y_hat = learner.predict(test)
            y_pred1 = np.add(y_pred1, y_hat.reshape(-1, 1))
        y_pred1=y_pred1/len(self.committe_predict)
        print("--------------------")
        print(y_pred1)
        print("--------------------")
        print(y_test1)
        print("--------------------")
        print(sqrt(MSE(y_test1, y_pred1)))
            
    def train(self, Committee):
        pool_idx = IndexCollection(np.arange(len(self.X)))
        start_idx = IndexCollection.random_sampling(pool_idx, rate=self.ratio)
        remain_idx = pool_idx.difference_update(start_idx)
        bag_idx, query_idx = [], []
        for learner in Committee:
            bag_idx.append(IndexCollection.random_sampling(start_idx, rate = .9))
        y_pred = np.zeros(self.y_test.shape)
        for iter in range(self.N):
            for i_bag, learner in enumerate(Committee): # Split training data bags
                bag_idx[i_bag].update(query_idx)
                X_start, y_start = self.X[bag_idx[i_bag]], self.y[bag_idx[i_bag]]
                if not query_idx:
                    learner.fit(X_start, y_start)
                elif query_idx: # We donot want to re-train, but update the model
                    learner.partial_fit(X_start, y_start)
                # Finished pre-training the comittee members, Now testing
                self.committe_predict.append(learner)
                y_hat = learner.predict(self.X_test)
                y_pred = np.add(y_pred, y_hat.reshape(-1, 1))
            # Test the result
            y_pred = y_pred / len(Committee)
            # Save the results
            self.loss.append(sqrt(MSE(self.y_test, y_pred)))
            self.sampleSize.append(len(start_idx.update(query_idx)))
            # Get the new query
            
            X_remain, y_remain = self.X[remain_idx], self.y[remain_idx]
            # Start to query
            # errors = np.zeros(len(y_remain))
            y_remain_repo = []
            for learner in Committee:
                # get overall losses suggetsed by the committee members
                y_remain_pred = learner.predict(X_remain)
                # errors = np.add(errors, RMSE(y_remain, y_remain_pred))
                y_remain_repo.append(list(y_remain_pred))
            # query_idx = get_query_idx(errors, remain_idx)
            query_idx = get_query_idx(y_remain_repo, remain_idx)
            remain_idx.difference_update(query_idx)













