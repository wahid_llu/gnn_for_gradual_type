import torch
import torch.nn.functional as F
from torch.nn import Linear, ReLU
from torch_geometric.nn import GCNConv, GAE, VGAE


class Net(torch.nn.Module):
    def __init__(self, NetArch):
        super(Net, self).__init__()
        self.conv1 = GCNConv(NetArch[0], 16)
        self.conv2 = GCNConv(16, NetArch[-1])

    def forward(self, data):
        x, edge_index = data.x, data.edge_index

        x = self.conv1(x, edge_index)
        x = F.relu(x)
        # x = F.dropout(x, training=self.training)
        x = self.conv2(x, edge_index)

        return F.log_softmax(x, dim=1)

class GCN(torch.nn.Module):
    def __init__(self, NetArch):
        super(GCN, self).__init__()
        self.conv1 = GCNConv(NetArch[0], NetArch[1])
        self.conv2 = GCNConv(NetArch[1], NetArch[2])
        self.fc1 = Linear(NetArch[2], NetArch[3])
        self.fc2 = Linear(NetArch[3], NetArch[-1])
        self.act1 = ReLU()
        self.act2 = ReLU()

    def forward(self, data):
            x, edge_index = data.x, data.edge_index
            x = self.conv1(x, edge_index)
            x = F.relu(x)
            x = self.conv2(x, edge_index)
            x = F.relu(x)
            logit = x
            x = self.fc1(x)
            x = self.act1(x)
            x = self.fc2(x)
            x = self.act2(x)
            return logit, F.log_softmax(x, dim=1)

class TACA(torch.nn.Module):
    def __init__(self, NetArch):
        super(TACA, self).__init__()
        self.conv1 = GCNConv(NetArch[0], NetArch[1])
        self.conv2 = GCNConv(NetArch[1], NetArch[-1])

    def forward(self, data):
        x, edge_index = data.x, data.edge_index

        x = self.conv1(x, edge_index)
        x = F.relu(x)
        logits = x
        # x = F.dropout(x, training=self.training)
        x = self.conv2(x, edge_index)

        return logits, F.log_softmax(x, dim=1)

class Encoder(torch.nn.Module):
    def __init__(self, in_channels, out_channels):
        super(Encoder, self).__init__()
        self.conv1 = GCNConv(in_channels, 2 * out_channels, cached=True)
        self.conv_mu = GCNConv(2 * out_channels, out_channels, cached=True)
        self.conv_logvar = GCNConv(
            2 * out_channels, out_channels, cached=True)

    def forward(self, x, edge_index):
        x = F.relu(self.conv1(x, edge_index))
        return self.conv_mu(x, edge_index), self.conv_logvar(x, edge_index)


class GCN_Predict(torch.nn.Module):
    def __init__(self, NetArch):
        super(GCN_Predict, self).__init__()
        self.conv = GCNConv(NetArch[1], NetArch[2])
        self.predict = Linear(NetArch[2], NetArch[3])

    def forward(self, data):
        x, edge_index = data.x, data.edge_index
        x = self.conv(x, edge_index)
        x = F.relu(x)
        return x, self.predict(x)